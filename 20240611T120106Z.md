On June 11, 2024, the following religious holidays are observed:

1. **St. Barnabas' Day**: This is a Christian feast day commemorating St. Barnabas, an early Christian disciple and missionary who is mentioned in the Acts of the Apostles.

2. **Dragon Boat Festival (Duanwu Festival)**: This is a traditional Chinese holiday that occurs on the 5th day of the 5th month of the lunar calendar, which falls on June 11 in 2024. It is celebrated with dragon boat races and eating zongzi (sticky rice dumplings).

These holidays reflect both Christian and traditional Chinese cultural observances on this date.

# Tue 11 Jun 12:01:06 CEST 2024 - What religious holidays are observed on 11 June 2024?